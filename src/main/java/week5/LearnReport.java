package week5;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {

	public static void main(String[] args) throws IOException {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(false);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		//test case level
		ExtentTest test = extent.createTest("Login","Create New leaftaps");
		test.assignCategory("smoke");
		test.assignAuthor("sadhiq");
		//test case step level
		test.pass("Browser launched sucessfully",MediaEntityBuilder.createScreenCaptureFromPath("./..snaps/img1.png").build());
		test.pass("The data DemosalesManager enterd sucessfully",MediaEntityBuilder.createScreenCaptureFromPath("./..snaps/img2.png").build());
		test.fail("The date crmsfa not enterd sucessfully",MediaEntityBuilder.createScreenCaptureFromPath("./..snaps/img3.png").build());
		test.pass("The element login button clicked sucessfully");
		extent.flush();
	
		

	}

}

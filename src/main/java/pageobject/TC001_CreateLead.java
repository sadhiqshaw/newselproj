package pageobject;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;


public class TC001_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
	testCaseName = "TC001_CreateLead";
	testCaseDesc = "Create lead";
	category = "testing";
	author = "sadhiq";
	excelFileName ="Parameterdata";
}
	@Test(dataProvider="fectchData")
	public void CreateLead(String username,String password,String company,String fname,String lname) {
		new LoginPage()
		.typeUserName(username)
		.typePassword(password)
		.clickLogin()
		.crmsfalink()
		.leads()
		.clickcreatelead()
		.typeCompanyName(company)
		.typefirstname(fname)
		.typelastname(lname)
		.submitpage()
		.clickLogout();
		

}
}

package pageobject;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MyHomePage extends ProjectMethods{
	
public MyHomePage() {
		
	}

public Myleadpage leads() {
	WebElement linklead = locateElement("xpath","//a[text()='Leads']");
	click(linklead);
	return new Myleadpage();

}
}

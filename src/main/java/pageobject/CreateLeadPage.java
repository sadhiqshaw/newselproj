package pageobject;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
	public CreateLeadPage() {
		
	}
	public CreateLeadPage typeCompanyName(String data) {
	WebElement company = locateElement("id","createLeadForm_companyName");
	type(company, data);
	return this;

}
	public CreateLeadPage typefirstname(String data) {
	WebElement fname = locateElement("id","createLeadForm_firstName");
	type(fname, data);
	return this;

}
	public CreateLeadPage typelastname(String data) {
	WebElement lname = locateElement("id","createLeadForm_lastName");
	type(lname, data);
	return this;
	

}
	public ViewLead submitpage() {
		WebElement elesubmit = locateElement("class","smallSubmit");
		click(elesubmit);
		return new ViewLead();
}
}

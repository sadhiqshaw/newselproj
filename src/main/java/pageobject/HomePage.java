package pageobject;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{
	
	public HomePage() {
		
	}
	
	public MyHomePage crmsfalink() {
	WebElement clickcrm = locateElement("linkText","CRM/SFA");
	click(clickcrm);
	return new MyHomePage();
	

}
}

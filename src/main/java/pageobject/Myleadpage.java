package pageobject;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class Myleadpage extends ProjectMethods {

	public Myleadpage() {
		

	}
	public CreateLeadPage clickcreatelead() {
		WebElement eleCreatelead = locateElement("xpath","//a[text()='Create Lead']");
		click(eleCreatelead);
		return new CreateLeadPage();
	
	

}
}

package pageobject;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods {
	
	public ViewLead() {
		
	}
	public  ViewLead verfyfname(String expectedText) {
		WebElement fname = locateElement("id","createLeadForm_firstName");
		verifyPartialText(fname, expectedText);
		return this ;
		
		
	}
	public LoginPage clickLogout() {
		WebElement elelogout =locateElement("linktext", "Logout");
		click(elelogout);
		return new LoginPage();
	}

}

package homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");

		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//table[@class='twoColumnForm']//img)[1]").click();
		
		Set<String> allwindow = driver.getWindowHandles();
		List<String> listwindow = new ArrayList<>();
		listwindow.addAll(allwindow);
		
		driver.switchTo().window(listwindow.get(listwindow.size()-1));
		
		driver.findElementByXPath("//a[@class='linktext']").click();
		
		driver.switchTo().window(listwindow.get(0));
		driver.findElementByXPath("(//table[@class='twoColumnForm']//img)[2]").click();
		
		 allwindow = driver.getWindowHandles();
		 listwindow = new ArrayList<>();
		listwindow.addAll(allwindow);
		
		driver.switchTo().window(listwindow.get(listwindow.size()-1));
		driver.findElementByLinkText("//a[@class='linktext'][2]").click();
		
		driver.switchTo().window(listwindow.get(0));
		
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		
		Alert alert= driver.switchTo().alert();
		alert.accept();
		
		
		
		
		
		
		
		
		
		//a[@class='linktext']
		
		
		

	}

}

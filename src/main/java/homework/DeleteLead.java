package homework;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DeleteLead {
	@Test()
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("//span[text()='Phone']").click();
		driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys("9008007001");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("(//table[@class='x-grid3-row-table']//tr//td//div//a[@class='linktext'])[1]").click();
		//driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a[@class='linktext'])[1]").click();
		//driver.findElementByXPath("((//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a[@class='linktext'])[1]").click();
		driver.findElementByXPath("//a[@class='subMenuButtonDangerous']").click();

	}

}

package week3.day2;

import org.openqa.selenium.chrome.ChromeDriver;

public class FindLeadsXpath {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://leaftaps.com/crmsfa/control/findLeads");
		//create lead
		driver.findElementByXPath("//input[@name='submitButton']").click();
		//find lead
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();

	}

}

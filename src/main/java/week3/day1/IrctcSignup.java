package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcSignup {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//Home Page
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		//uername
		driver.findElementById("userRegistrationForm:userName").sendKeys("hameedh");
		//pasword
		driver.findElementById("userRegistrationForm:password").sendKeys("joya");
		//confrirm password
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("joya");
		
		//sec question dropdown
		WebElement src = driver.findElementById("userRegistrationForm:securityQ");
		Select dropdown = new Select(src);
		dropdown.selectByVisibleText("What was the name of your first school?");
		//answer
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("srikrishh");
		
		//preflangugage
		WebElement src1 = driver.findElementById("userRegistrationForm:prelan");
		Select dropdown1 = new Select(src1);
		dropdown1.selectByVisibleText("English");
		//first name
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Hameedh");
		//latname
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Shaik");
		
		//radio button male
		
		driver.findElementById("userRegistrationForm:gender:0").click();
		//radio martial
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		//dob
		WebElement src2 = driver.findElementById("userRegistrationForm:dobDay");
		Select dropdowndob = new Select(src2);	
		dropdowndob.selectByVisibleText("07");
		
		WebElement src3 = driver.findElementById("userRegistrationForm:dobMonth");
		Select dropdownmonth = new Select(src3);
		dropdownmonth.selectByVisibleText("APR");
		
		WebElement src4 = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select dropdownyear = new Select(src4);
		dropdownyear.selectByVisibleText("1998");
		
		//occupation
		WebElement src5 =driver.findElementById("userRegistrationForm:occupation");
		Select dropoccup = new Select(src5);
		dropoccup.selectByVisibleText("Private");
		//aadharno
		driver.findElementById("userRegistrationForm:uidno").sendKeys("234412345678");
		//panno
		driver.findElementById("userRegistrationForm:idno").sendKeys("wert2345k");
		
		//country
		WebElement src6 = driver.findElementById("userRegistrationForm:countries");
		Select dropcountry = new Select(src6);
		dropcountry.selectByVisibleText("India");
		
		//email
		driver.findElementById("userRegistrationForm:email").sendKeys("Hameedh.h@gmail.com");
		//mobile
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9677149789");
		
		//nationality
		WebElement src7 =driver.findElementById("userRegistrationForm:nationalityId");
		Select dropnat = new Select(src7);
		dropnat.selectByVisibleText("India");
		
		
		
		
	
				
		
		
		
				

	}

}

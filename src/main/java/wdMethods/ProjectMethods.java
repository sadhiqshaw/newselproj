package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import framework.ReadExcel;

public class ProjectMethods extends SeMethods{
	@BeforeSuite
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass
	public void beforeClass() {
		startTestCase();
	}
	@Parameters({"url"})
	
	@BeforeMethod
	public void login(String url) {
		startApp("chrome", url);
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);*/
	}

	@AfterMethod(groups = {"common"})
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	@DataProvider(name= "fectchData")
	public Object[][] fectchData() throws IOException{
		
		
		
		return ReadExcel.getExcelData(excelFileName);
	}
	
	
	
	
	
	
	
	
}

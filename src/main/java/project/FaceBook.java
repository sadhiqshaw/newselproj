package project;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class FaceBook extends ProjectMethods{
	
	
	@BeforeTest
	public void setDate() {
		testCaseName = "FaceBook";
		testCaseDesc = "Facebookpage";
		category = "smoke";
		author ="sadhiq";
		
	}
	
	
	
	@Test
	public void facebook() {
		startApp("chrome","https://www.facebook.com/");
		//input[@id='email']
		WebElement email = locateElement("xpath","//input[@id='email']");
		type(email,"shaiksadhiq@gmail.com");
		
		WebElement password = locateElement("xpath","//input[@id='pass']");
		type(password,"althafsk@123");
		
		WebElement login = locateElement("xpath","//input[@type='submit']");
		click(login);
		

		WebElement wb4 = locateElement("class","_1frb");
		type(wb4,"TestLeaf");
		
		WebElement search = locateElement("xpath","//button[@data-testid='facebar_search_button']");
		click(search);
		
		
		WebElement like = locateElement("xpath","(//button[@type='submit'])[2]");
		String temp=like.getText();
		if(temp.equals("Like")) {
			click(like);
			
		}else {
			System.out.println("Test leaf alredy liked");
		}
		
		
		WebElement clicktestleaf = locateElement("xpath","//div[text()='TestLeaf']");
		click(clicktestleaf);
		
		String title = getTitle();
		System.out.println("The given title is");
		
		
	}
}

package framework;

import wdMethods.SeMethods;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;

public class TC03_DeleteLead extends SeMethods {
	
	@Parameters({"url","username","password"})
	@Test
	public void delete(String url,String usern,String Pass) {
		startApp("chrome",url);
		WebElement we1 = locateElement("id","username");
		type(we1,usern);
		WebElement we2 = locateElement("id","password");
		type(we2,Pass);
		WebElement we3 = locateElement("class","decorativeSubmit");
		click(we3);
		
		WebElement we4 = locateElement("linkText","CRM/SFA");
		click(we4);
		
		
		WebElement we9 = locateElement("linkText","Leads");
		click(we9);
		

		WebElement we10 = locateElement("linkText","Find Leads");
		click(we10);
		
				
		WebElement we11 = locateElement("xpath","(//a[@class='linktext'])[4]");
		click(we11);
				
		WebElement we12 = locateElement("class","subMenuButtonDangerous");
		click(we12);
		
	}

}

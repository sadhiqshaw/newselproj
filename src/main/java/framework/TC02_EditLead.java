package framework;

import wdMethods.SeMethods;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;

public class TC02_EditLead extends SeMethods{

	@Test(dataProvider="editdata")
	public  void edit(String fname, String company) {
		
		startApp("chrome","http://leaftaps.com/opentaps/control/main");
		WebElement we1 = locateElement("id","username");
		type(we1,"DemoSalesManager");
		WebElement we2 = locateElement("id","password");
		type(we2,"crmsfa");
		WebElement we3 = locateElement("class","decorativeSubmit");
		click(we3);
		
		WebElement we4 = locateElement("linkText","CRM/SFA");
		click(we4);
		
		
		WebElement we9 = locateElement("linkText","Leads");
		click(we9);
		

		WebElement we10 = locateElement("linkText","Find Leads");
		click(we10);
		
		WebElement we11 = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(we11,fname);
		

		WebElement we12 = locateElement("xpath","(//a[@class='linktext'])[4]");
		click(we12);
		

		WebElement we13 = locateElement("xpath","(//a[@class='subMenuButton'])[3]");
		click(we13);
		

		WebElement we14 = locateElement("id","updateLeadForm_companyName");
		type(we14,company);
		
		
		WebElement we15 = locateElement("xpath","(//input[@class='smallSubmit'])[1]");
		click(we15);
		
		closeBrowser();
	}
	
	
	@DataProvider(name = "editdata")
	public Object[][] editdatap(){
		Object[][] data = new Object[2][2];
		data[0][0] = "subbu";
		data[0][1] = "IBM";
		
		data[0][0] = "mitra";
		data[0][1] = "IQW";
		
		return data;
		
	}

}

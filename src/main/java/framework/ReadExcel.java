package framework;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getExcelData(String excelFileName) throws IOException {
		// Create object for XSSFworkbook 
		XSSFWorkbook wbook =new XSSFWorkbook("./data/"+excelFileName+".xlsx");
		// get sheet of index
		XSSFSheet sheet = wbook.getSheetAt(0);
		//to get last row num in a sheet
		int rowCount = sheet.getLastRowNum();
		System.out.println("Row count ="+rowCount);
		
		int columnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Column count ="+columnCount);
		Object[][] data = new Object[rowCount][columnCount];
		for(int j = 1; j <= rowCount;j++) {
			XSSFRow row = sheet.getRow(j);
			for(int i =0; i< columnCount; i++) {
				XSSFCell cell = row.getCell(i);
				//String celValue = cell.getStringCellValue();
				data[j-1][i] = cell.getStringCellValue();
			}
			
		}
		return data;
		
		

	}

}

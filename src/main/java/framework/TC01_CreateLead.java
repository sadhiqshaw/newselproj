package framework;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import wdMethods.SeMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;



public class TC01_CreateLead extends SeMethods {


	@Test(dataProvider="fectchData")
	public void lead(String compny,String fname,String lname) {
		startApp("chrome","http://leaftaps.com/opentaps/");
		
		
		WebElement wb1 = locateElement("id","username");
		System.out.println(" wb1 " + wb1);
		
		type(wb1, "DemoSalesManager");
		System.out.println(" wb1 " + wb1);
		
		WebElement wb2 = locateElement("id","password");
		type(wb2, "crmsfa");
		WebElement wb3 = locateElement("class","decorativeSubmit");
		click(wb3);
		WebElement wb4 = locateElement("linkText","CRM/SFA");
		click(wb4);
		WebElement wb5 = locateElement("linkText","Create Lead");
		click(wb5);
		WebElement wb6 = locateElement("id","createLeadForm_companyName");
		type(wb6, compny);
		WebElement wb7 = locateElement("id","createLeadForm_firstName");
		type(wb7, fname);
		WebElement wb8 = locateElement("id","createLeadForm_lastName");
		type(wb8, lname);
		WebElement wb9 = locateElement("class","smallSubmit");
		click(wb9);
	}
		
	

}


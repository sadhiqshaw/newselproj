package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Frames {

	public static void main(String[] args) {
		//https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		WebDriver frame = driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		
		Alert alert= driver.switchTo().alert();
		System.out.println(alert.getText());
		alert.sendKeys("sadhiq");
		alert.accept();
		String l = driver.findElementById("demo").getText();
		System.out.println(l);
		driver.close();
		
		
		


	}

}

package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		System.out.println(driver.getTitle());
		
		Set<String> allwindow = driver.getWindowHandles();
		List<String> listwindow = new ArrayList<>();
		listwindow.addAll(allwindow);
		
		driver.findElementByXPath("//a[text()='Contact Us']").click();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		
		File screenshotAs = driver.getScreenshotAs(OutputType.FILE);
		File destination = new File("./snaps/img.png");
		FileUtils.copyFile(screenshotAs, destination);
		driver.switchTo().window(listwindow.get(0)).close();
		
		
		
		
		
		
		
		
		

	}

}

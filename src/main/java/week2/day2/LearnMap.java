package week2.day2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LearnMap {

	public static void main(String[] args) {
		Scanner scr =new Scanner(System.in);
		System.out.println("Enter your name:");
		String str = scr.next();
		char[] ch = str.toCharArray();
		
		Map<Character,Integer> map =new HashMap<Character,Integer>();
		
		for(char c : ch) {
			if(map.containsKey(c)) {
				int val = map.get(c)+1;
				map.put(c, val);
			}
			System.out.println();
				
			}
		}
		

	}



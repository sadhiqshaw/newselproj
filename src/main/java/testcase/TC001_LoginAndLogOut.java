package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethodsold.SeMethods;

public class TC001_LoginAndLogOut extends ProjectMethods{
	
	@BeforeTest
	public void setDate() {
		testCaseName = "TC001_LoginAndLogOut";
		testCaseDesc = "Create a new Lead";
		category = "smoke";
		author ="sadhiq";
		
	}
	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);
	}
	
}








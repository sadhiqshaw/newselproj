package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethodsold.SeMethods;

public class TC002_DropDown extends SeMethods{
	
	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement elelink = locateElement("linktext", "CRM/SFA");
		click(elelink);
		
		WebElement elelead = locateElement("linktext", "Create Lead");
		click(elelead);
		
		WebElement dropdown = locateElement("createLeadForm_dataSourceId");
		selectDropDownUsingText(dropdown,"Direct Mail");
		
		WebElement eleFindlead = locateElement("linktext", "Find Leads");
		click(eleFindlead);
		
		
		
		
		
		
		
		
		
		
		WebElement eleLogout = locateElement("linktext", "Logout");
		click(eleLogout);

}
}
